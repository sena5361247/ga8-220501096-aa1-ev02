const {Router} = require("express")
const ctrEqFV = require("../controllers/equiposfv.controller")
const routerEquiposFV = Router()


// Ruta para la creación de un producto
routerEquiposFV.post('/', ctrEqFV.agregarEquiposFV)

// Ruta para obtener todos los productos registrados
routerEquiposFV.get('/', ctrEqFV.obtenerEquiposFV)

// Ruta para obtener un producto en particular
//routerEquiposFV.get('/:No_Parte', ctrEqFV.obtenerunEquiposFV)
routerEquiposFV.get('/:Voltaje', ctrEqFV.obtenervoltajeEquiposFV)

// Ruta para actualizar un producto completo
routerEquiposFV.put('/:id', ctrEqFV.actualizarEquiposFV)

// Ruta para borrar un producto en particular
routerEquiposFV.delete('/:id', ctrEqFV.eliminarEquiposFV)

module.exports = routerEquiposFV