const express = require("express")
const routerEquiposFV  = require("./routes/equiposfv.routes")
const conexionDB = require("./db.conexion")
const app = express()

// conexion DB
conexionDB()

//settings
app.set("name","REST-API-NODEJS-EQUIPOSFV")
app.set("port",process.env.port || 3300)


//midleware
app.use (express.json())


app.use("/api/equiposfv",routerEquiposFV)


module.exports = app