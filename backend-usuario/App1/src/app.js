const express = require("express")
const routerUsuario  = require("./routes/usuarios.routes")
const conexionDB = require("./database")
const app = express()

// conexion DB
conexionDB()

//settings
app.set("name","REST-API-NODEJS")
app.set("port",process.env.port || 3500)


//midleware
app.use (express.json())


app.use("/api/usuarios",routerUsuario)


module.exports = app