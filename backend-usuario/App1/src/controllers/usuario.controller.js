const Usuario = require("../models/Usuarios")

var x, y;

exports.obtenerUsuario= async (req, res) => {
try {
  const usuarios = await Usuario.find()
  res.json(usuarios)
} catch (error) {
  res.json(error)  
}
  }

exports.updateusuarioid= async (req, res) => {
  try {
    const {usuario,correo,password} = req.body

    if (usuario && correo && password){
      console.log({usuario,correo,password})      
      const usuarios = await Usuario.findOne({usuario})
      x = usuarios._id
      console.log(x)
      if (!usuarios) {
        res.json(false)          
      } else {     
        const usuarios = await Usuario.findOne({correo})
        y = usuarios._id
        console.log(y)
        if (!usuarios) {
        res.json(false)     
      } else {        
          if (x=y) {
            const usuarios = await Usuario.findByIdAndUpdate(x,{password},{new:true})
            res.json(usuarios)  
          } else {
            res.json({isOk: false, msj: "No se encontro coincidencia en la base de datos"}) 
          }
        }
   
      }       

    } else {
      res.json({isOk: false, msj: "Se requieren datos"})      
    }     

  } catch (error) {
  res.json(error)
    }

  }

exports.obtenerunUsuario= async (req, res) => {
  try {
    const {usuario,password} = req.body

    if (usuario && password){      
      const usuarios = await Usuario.findOne({usuario})
      if (!usuarios) {
        //res.json({isOk: false, msj:"Error en la Autenticacion"})
        res.json(false)          
      } else {
        const usuarios = await Usuario.findOne({password})
        if (!usuarios) {
        //res.json({msj:"Error en la Autenticacion"})
        res.json(false)     
      } else {
        //res.json({isOk: true,msj:"Autenticacion Satisfactoria"})
        res.json(true) 
        }
        
      }       
    
    } else {
      res.json({isOk: false, msj: "Se requieren datos"})      
    }
    
  } catch (error) {
  res.json(error)    
  }

  }

exports.agregarUsuario = async (req, res) => {
  try {
    const {nombre, apellido,cedula,correo,usuario,password} = req.body
    console.log(nombre)

    if (nombre && apellido && cedula && correo && usuario && password){
      const nuevoUsuario = new Usuario({nombre, apellido,cedula,correo,usuario,password})
      await nuevoUsuario.save()
      res.json({msj:"documento insertado satisfactoriamente"})
    } else {
      res.json({isOk: false, msj: "Los datos son requeridos"})
    }
    
  } catch (error) {
    res.json(error)   
  }
  }

exports.actualizarUsuario = async (req, res) => {
try {
  const _id = req.params.id
  console.log(_id)
  const data = req.body
  

  if (_id && data){
    await Usuario.findByIdAndUpdate(_id, data)    
    res.json("Registro actualizado")    
  } else {
    res.json({msj:"Registro no actualizado"})     
  }
} catch (error) {
  res.json(error)
}
  }

    
exports.eliminarUsuario = async (req, res) => {
try {

  const _id = req.params.id  
  console.log(_id)
  await Usuario.findByIdAndDelete(_id)
  res.status(200).json({msj:"id recibida para eliminiar usuario", isOk: true})
} catch (error) {
  res.status(500).json(error)
}
  }


