const {Router} = require("express")
const ctrUsu = require("../controllers/usuario.controller")
const routerUsuario = Router()


// Ruta para la creación de un usuario
routerUsuario.post('/', ctrUsu.agregarUsuario)

// Ruta para obtener todos los usuario registrados
routerUsuario.get('/', ctrUsu.obtenerUsuario)

// Ruta para obtener un usuario en particular
routerUsuario.post('/:validar', ctrUsu.obtenerunUsuario)

// Ruta para actualizar un usuario completo
routerUsuario.put('/:id', ctrUsu.actualizarUsuario)

// Ruta para actualizar el password
routerUsuario.post('/validar/edit', ctrUsu.updateusuarioid)

// Ruta para borrar un usuario en particular
routerUsuario.delete('/:id', ctrUsu.eliminarUsuario)

module.exports = routerUsuario